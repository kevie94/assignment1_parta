/**
 * Created by Kevin on 26/09/2016.
 */
public class Module {
    private int classSize;
    private String moduleName;
    private String ID;
    private Student[] studentList = null;

    public Module(String moduleName, String ID) {
        this.moduleName = moduleName;
        this.ID = ID;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getID() {
        return ID;
    }

    public Student[] getStudentList() {
        return studentList;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setStudentList(Student[] studentList) {
        this.studentList = studentList;
    }

    public void addStudent(Student student) {
        if(this.studentList == null) {
            this.studentList = new Student[100];
        }
        this.studentList[classSize] = student;
        classSize++;
    }

    public void printModule() {
        System.out.println(this.getModuleName() + " " + this.getID());
    }
}
