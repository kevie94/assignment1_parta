import org.joda.time.DateTime;

/**
 * Created by Kevin on 26/09/2016.
 */

public class Course {
    private String courseName;
    private Module[] moduleList = null;
    private int numModules = 0;
    private DateTime startDate;
    private DateTime endDate;

    public Course(String courseName, int startYear, int startMonth, int startDay, int endYear, int endMonth, int endDay) {
        this.courseName = courseName;
        this.startDate = new DateTime(startYear, startMonth, startDay, 0, 0);
        this.endDate = new DateTime(endYear, endMonth, endDay, 0, 0);
    }

    public String getCourseName() {
        return courseName;
    }

    public Module[] getModuleList() {
        return moduleList;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setModuleList(Module[] moduleList) {
        this.moduleList = moduleList;
    }

    public void addModule(Module module) {
        if(this.moduleList == null) {
            this.moduleList = new Module[20];
        }
        this.moduleList[numModules] = module;
        numModules++;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public void printAll() {
        System.out.println("Displaying module info for course: " + this.courseName);
        for(Module mod: moduleList){
            if(mod != null) {
                System.out.println("Students registered for: " + mod.getModuleName() + " - " + mod.getID());

                for (Student stu : mod.getStudentList()) {
                    if(stu != null) {
                        System.out.println(stu.getName() + " - " + stu.returnUsername());
                    }
                }
            }
        }
    }

    public void printCourse() {
        System.out.println(this.getCourseName() + " " + this.getStartDate().toString() + " " + this.getEndDate().toString());
    }
}
