
/**
 * Created by Kevin on 26/09/2016.
 */
public class Student {
    private String name;
    private int age;
    private String ID;
    private String username;

    public Student(String name, int age, String ID){
        this.setName(name);
        this.setAge(age);
        this.setID(ID);
        this.getUsername();
    }


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getID() {
        return ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void getUsername() {
        this.username = this.getName() + String.valueOf(this.getAge());
    }

    public String returnUsername() {
        return this.username;
    }

    public void printStudent() {
        System.out.println(this.getName() + " " + this.getID() + " " + this.returnUsername() + " " + this.getAge());
    }
}
